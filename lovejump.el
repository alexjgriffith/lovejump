;;; lovejump.el --- Jump through love2d stack traces

;; Copyright (C) 2011 Free Software Foundation, Inc.

;; Author: Alexander Griffith <griffitaj@gmail.com>
;; Version: 0.1.0
;; Keywords: love2d, fennel
;; Package-Requires: ((emacs "24.4"))
;; Homepage: https://gitlab.com/alexjgriffith/lovejump.el

;; This file is not part of GNU Emacs.

;; This file is part of lovejump.el.

;; lovejump.el is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; lovejump.el is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with lovejump.el.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; This package provides a minor mode that lets you
;; jump through trace back calls in love2d when using
;; a fennel repl.

;; To start the stack trace call `lovejump-start' in an
;; love2d *inferior lisp* buffer.

;; ;; Add the following to your init file
;; (add-to-list 'load-path "/path/to/lovejump/")
;; (require 'lovejump)
;; (add-hook 'fennel-mode 'lovejump-mode)
;; (add-hook 'inferior-lisp-mode 'lovejump-mode)
;;

;;; Code:

(defvar lovejump-project-auto 't
  "When true `lovejump-project-directory' is set by `lovejump-start'.
The directory is set to the default directory and the
project-name is set to the last directory in the path.")

(defvar lovejump-project-directory ""
  "The root of the love project.")

(defvar lovejump-project-name ""
  "The name of the love project.")

(defvar lovejump-inferior-buffer-string "*inferior-lisp*"
  "The name of the *inferior-lisp* buffer.")

(defvar lovejump--start-of-stacktrace "Error: "
  "String used to identify the start of the love2d stack trace.

Note, this may need to be changed for this to work with different
versions of love.")

(defvar lovejump--end-of-stacktrace "xpcall"
  "String used to identify the end of the love2d stack trace.

Note, this may need to be changed for this to work with different
versions of love.")

(defvar lovejam--buffer-before-start nil
  "Holds the buffer and position from before the start call.")

(defvar lovejump--description "nil"
  "The description of the error that caused the stack trace.")

(defvar lovejump--list nil
  "Current stack trace list of files and lines.")

(defvar lovejump--index 0
  "Current index of the `lovejump-list'.")

(defvar lovejump--max 0
  "Length of `lovejump-list'.")

(defun lovejump--get-file-at-point ()
  "Get the file and line number at the top of the stack trace."
  (save-excursion
    (re-search-backward lovejump--start-of-stacktrace)
    (re-search-forward ":" nil nil 2)
    (let ((start (+ (length lovejump-project-name ) (re-search-backward lovejump-project-name)))
          (file-end (re-search-forward ":"))
          (number-end (re-search-forward ":")))
      (list
       (buffer-substring-no-properties (+ start 0) (- file-end 1))
       (buffer-substring-no-properties (+ file-end 0) (- number-end 1))))))

(defun lovejump--get-file-at-point-specific ()
  "Get the file and line number on the current-line."
  (save-excursion
    (re-search-forward ":")
    (let ((start (+ (length lovejump-project-name ) (re-search-backward lovejump-project-name)))
          (file-end (re-search-forward ":"))
          (number-end (re-search-forward ":")))
      (list
       (buffer-substring-no-properties (+ start 0) (- file-end 1))
       (buffer-substring-no-properties (+ file-end 0) (- number-end 1))))))

(defun lovejump--jump-to-file (file directory line)
  "Open FILE in DIRECTORY at LINE."
  (find-file (concat directory file))
  (goto-line (string-to-number line)))

(defun lovejump--get-end-of-stacktrace ()
  "Find the end of the stacktrace.

Relies on `lovejump--end-of-stacktrace' variable, which may
have to be changed based on the version of love you are
useing"
  (save-excursion
    (let ((end (re-search-forward lovejump--end-of-stacktrace nil 't)))
      (unless end
        (error (format "%s missing from the end of stacktrace."
                       lovejump--end-of-stacktrace)))
      end)))

(defun lovejump--get-start-of-stacktrace ()
  "Find the start of the stacktrace.

Relies on `lovejump--start-of-stacktrace' variable, which may
have to be changed based on the version of love you are
useing"
  (save-excursion
    (let ((start (re-search-backward lovejump--start-of-stacktrace nil 't)))
      (unless start
        (error (format "%s missing from the start of stacktrace."
                       lovejump--start-of-stacktrace)))
      start)))

(defun lovejump--extract-most-of-stacktrace ()
  "Extract most of the stack trace into a string.

This function will miss anything after a xpcall."
  (buffer-substring-no-properties (lovejump--get-start-of-stacktrace)
                                  (lovejump--get-end-of-stacktrace)))

(defun lovejump--get-error-description ()
  "Get the error discription from the top of the stack trace."
  (save-excursion
    (goto-char (point-max))
    (re-search-backward lovejump--start-of-stacktrace)
    (let ((start (re-search-forward ":" nil nil 3))
          (end (re-search-forward "$")))
      (buffer-substring (+ start 1) end))))

(defun lovejump--pull-list-of-relevant-traces ()
  "Pull the relevant stack trace lines into a list.

Only return traces that have the `lovejump-project-name' in them.
I.e., this call ignores system traces."
  (save-excursion
    (with-current-buffer (get-buffer lovejump-inferior-buffer-string)
      ;; this is a work around to find the most recent
      ;; stack trace
      (goto-char (point-max))
      (unless (re-search-backward lovejump--start-of-stacktrace nil 't)
        (error
         "%s, the start of the stack trace, could not be found in %s"
         lovejump--start-of-stacktrace
         lovejump-inferior-buffer-string))
      (goto-char (+ (point) 10))
      (let (files
            (key-name lovejump-project-name)
            (str (lovejump--extract-most-of-stacktrace)))
        (with-temp-buffer
          (insert str)
          (setq lovejump--description (lovejump--get-error-description))
          (while (re-search-backward key-name nil 't)
            (push (lovejump--get-file-at-point-specific) files)))
        (reverse (rest files))))))

(defun lovejump--open-file ()
  "Open the current `lovejump--index' file."
  (destructuring-bind (filename line)
      (elt lovejump--list lovejump--index)
    (lovejump--jump-to-file filename lovejump-project-directory line)))

(defun lovejump-next ()
  "Open the next stack trace file at line."
  (interactive)
  (incf lovejump--index)
  (if (>= lovejump--index lovejump--max)
      (progn
        (setq lovejump--index  lovejump--max)
        (lovejump-reset)
        (message "At the start of the stack trace."))
    (progn
        (lovejump--open-file)
        (message (format "%s of %s. %s"
                         (+ 1 lovejump--index)
                         lovejump--max
                         lovejump--description
                         )))))

(defun lovejump-previous ()
  "Open the previous stack trace file at line."
  (interactive)
  (decf lovejump--index)
  (if (< lovejump--index 0)
      (progn
        (setq lovejump--index -1)
        (lovejump-reset)
        (message "At the end of the stack trace."))

    (progn
        (lovejump--open-file)
        (message (format "%s of %s. %s"
                         (+ 1 lovejump--index)
                         lovejump--max
                         lovejump--description
                         )))))

(defun lovejump--set-previous-buffer ()
  "Set the buffer and position before `lovejump-start' is called."
  (let ((pos (point))
        (buffer (current-buffer)))
    (setq lovejam--buffer-before-start (list buffer pos))))

(defun lovejump-reset ()
  "Go back to the buffer from before `lovejump-start' was called."

  (destructuring-bind (buffer pos) lovejam--buffer-before-start
    (switch-to-buffer buffer)
    (goto-char pos)))

;;;###autoload
(defun lovejump-start ()
  "Update the stack trace information and open the first file at line.

To be called from the *inferior-lisp* buffer"
  (interactive)
  (when lovejump-project-auto
    (setq lovejump-project-directory (directory-file-name default-directory))
    (setq lovejump-project-name (car (last (split-string-and-unquote (pwd) "/")))))
  (lovejump--set-previous-buffer)
  (setq lovejump--list (lovejump--pull-list-of-relevant-traces))
  (setq lovejump--index -1) ;; will be incremented by lovejump-next
  (setq lovejump--max (length lovejump--list))
  (lovejump-next))

(defun lovejump-debug ()
  "Open the file at point.

To be called from the *inferior-lisp* buffer"
  (interactive)
  (destructuring-bind (filename line)
      (lovejump--get-file-at-point-specific)
    (lovejump--jump-to-file filename lovejump-project-directory line)))

;;;###autoload
(define-minor-mode lovejump-mode
  "For jumping through love2d stack traces."
  :lighter " lovejump"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "M-s M-s") 'lovejump-start)
            (define-key map (kbd "M-n") 'lovejump-next)
            (define-key map (kbd "M-p") 'lovejump-previous)
            map))

(provide 'lovejump)
;;; lovejump.el ends here
